console.log("Activity in JS Arrays");

let student = [];
console.log(student);



function addStudent(name){
	student.push(name)
}

addStudent("John");
addStudent("Jane");
addStudent("Joe");

console.log(student);



function countStudents(){
	

	return `There are ${student.length} students currently enrolled`
}

console.log(countStudents())





function printStudents(){
	student.sort()
	

	student.forEach(function(name){
		console.log(name)
	})
}

printStudents()




function findStudent(keyword){
	
	let word = keyword.toLowerCase();

	
	let match = student.filter(function(name){
		
		let lcName = name.toLowerCase();

		return lcName.includes(word)
	})
	console.log(match);


	
	if(match.length === 1){
		console.log(`${match} is an enrollee`);

	
	}else if(match.length > 1){
		console.log(`${match} are enrollees`);

	
	} else {
		console.log(`${keyword} is not an enrollee.`);
	}
}

findStudent("ane");




function addSection(section){

	let sectionedStudents = student.map(function(name){
		

		return `${name} - Section ${section}`
	})

	console.log(sectionedStudents)
}

addSection("A");




function removeStudent(name){
	

	
	let firstLetter = name.slice(0, 1).toUpperCase();
	let remainingLetter = name.slice(1)

	let capName = firstLetter + remainingLetter


	
	let studentIndex = student.indexOf(capName);
	console.log(studentIndex);

	
	if(studentIndex !== -1){
		student.splice(studentIndex, 1);

		
		console.log(`${capName} was removed from the student’s list`)
	}
}

removeStudent("John");

console.log(student);
